# PyBot
#### This Project uses the rewrite version of [discord.py](https://github.com/Rapptz/discord.py/tree/rewrite) as well as [Python 3.6](https://www.python.org/downloads/release/python-360/) or higher. Please keep this in mind when using the bot.

## Acknowledgements
Thanks to
- [Yellowfin](https://github.com/SegmentationViolation) for hosting the bot on his server.
